package messer;

public class Coordinate {
	private double latitude;
	private double longitude;
	
	//TODO: Constructor, Getter/Setter and toString()
	Coordinate(double latitude,double longitude){
		this.latitude = latitude;
		this.longitude = longitude;
	}
	//Lat
	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	//Long
	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	//toString
	public String toString() {
		return "Latitude: " + getLatitude() + " | Longitude: " + getLongitude();
	}
	
	
}