package senser;

import java.util.ArrayList;
import java.util.Observable;

import jsonstream.*;

public class Senser extends Observable implements Runnable
{
	private static boolean lab1 = false;
	private static boolean range = true;
	PlaneDataServer server;

	public Senser(PlaneDataServer server)
	{
		this.server = server;
	}

	private String getSentence()
	{
		String list = server.getPlaneListAsString();
		
		return list;
	}
	
	public void run()
	{
		ArrayList<AircraftSentence> jsonAircraftList;
		String aircraftList;
		
		//TODO: Create factory and display object 
		AircraftSentenceDisplay display = new AircraftSentenceDisplay();
		AircraftSentenceFactory factory = new AircraftSentenceFactory();

		
		while (true)
		{
			aircraftList = getSentence();
			
			if(aircraftList == null || aircraftList.length() == 0)
				continue;
			
			//TODO: get aircraft list from factory and display plase jsons 
			jsonAircraftList = factory.fromAircraftJson(aircraftList);
			
			
			if (range) System.out.println("Current Aircrafts in range " + jsonAircraftList.size());
			
			
			for(int i=0; i<jsonAircraftList.size();i++)
			{
				// Display the sentence in Lab 1; disable for other labs
				AircraftSentence sentence = new AircraftSentence(jsonAircraftList.get(i));
				if (lab1) display.display(sentence);
				
				// Notify all observers
				setChanged();
				notifyObservers(sentence);
			}
			if (lab1) System.out.println();
			if (lab1) try {
				Thread.sleep(5000);
			} catch (InterruptedException e) { }
			
		}		
	}
}

